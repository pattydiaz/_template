# Project Template

Helpful/necessary workflow installations:

- [Visual Studio Code](https://code.visualstudio.com/)
- [Filezilla](https://filezilla-project.org/)
- [Sourcetree](https://www.sourcetreeapp.com/)
- [MAMP](https://www.mamp.info/en/downloads/)
- [Node.js](https://nodejs.org/)
- [npm](https://docs.npmjs.com/getting-started/installing-node)
- [Grunt](https://gruntjs.com/)
- [npm-watch](https://www.npmjs.com/package/npm-watch)
- [Browsersync](https://www.browsersync.io/docs/grunt)
- [WordPress](https://wordpress.org/)

## Setup

### MAMP

Change Document Root to:

> /Desktop/Projects

Allow MAMP to use virtual hosts by opening the following file:

> /Applications/MAMP/conf/apache/httpd.conf

And remove the comment on the line under `# Virtual hosts` to look like this:

```
# Virtual hosts
Include /Applications/MAMP/conf/apache/extra/httpd-vhosts.conf
```

To create a virtual host, add an entry to your hosts file:

> /private/etc/hosts

```
127.0.0.1     local.[domain].com
```

Then point to where your files are located in:

> /Applications/MAMP/conf/apache/extra/httpd-vhosts.conf

```
<VirtualHost *:80>
  DocumentRoot "/Users/[user]/[path]/[to]/[folder]/target"
  ServerName local.[domain].com
</VirtualHost>
```

Make sure MAMP settings look like this:

- Apache Port: 80
- Nginx Port: 8080
- MySQL Port: 3306

** Relaunch MAMP/clear browser cache every time you create a new project

### New Project

- Download and rename the _template folder
- In **package.json**, edit any instance of `project-name`
- In **gruntfile.js**, edit the `project` and `theme` variables
- In **.gitignore**, edit the `theme` or `ma-projectname-theme`
- Run `sudo npm install`

## Build

- Launch MAMP
- Run `grunt dev` to build the project
- Run `grunt prod` when ready to migrate to FTP