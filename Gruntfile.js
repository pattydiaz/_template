const sass = require("node-sass");
const host = "projectname";
const theme = "ma-projectname-theme";

module.exports = function (grunt) {
  grunt.initConfig({
    sass: {
      options: {
        implementation: sass,
      },
      build: {
        files: [
          {
            src: ["src/scss/admin.scss"],
            dest: "target/wp-content/themes/" + theme + "/admin.css",
          },
          {
            src: ["src/scss/style.scss"],
            dest: "target/wp-content/themes/" + theme + "/style.css",
          },
        ],
      },
    },
    concat: {
      css: {
        files: [
          {
            src: ["src/css/*.css"],
            dest: "target/wp-content/themes/" + theme + "/app.css",
          },
        ],
      },
      app: {
        files: [
          {
            src: ["src/js/global/*.js"],
            dest: "target/wp-content/themes/" + theme + "/app.js",
          },
        ],
      },
      script: {
        files: [
          {
            src: [
              "src/js/src/global.js",
              "src/js/src/**/*.js",
              "src/js/src/*.js",
              "!src/js/src/*init.js",
              "src/js/src/init.js",
            ],
            dest: "target/wp-content/themes/" + theme + "/main.js",
          },
        ],
      },
    },
    cssmin: {
      app: {
        files: [
          {
            src: ["target/wp-content/themes/" + theme + "/app.css"],
            dest: "target/wp-content/themes/" + theme + "/app.css",
          },
          {
            src: ["target/wp-content/themes/" + theme + "/admin.css"],
            dest: "target/wp-content/themes/" + theme + "/admin.css",
          },
          {
            src: ["target/wp-content/themes/" + theme + "/style.css"],
            dest: "target/wp-content/themes/" + theme + "/style.css",
          },
        ],
      },
    },
    uglify: {
      js: {
        files: [
          {
            src: ["target/wp-content/themes/" + theme + "/app.js"],
            dest: "target/wp-content/themes/" + theme + "/app.js",
          },
          {
            src: ["target/wp-content/themes/" + theme + "/main.js"],
            dest: "target/wp-content/themes/" + theme + "/main.js",
          },
        ],
      },
    },
    browserSync: {
      dev: {
        bsFiles: {
          src: [
            "target/wp-content/themes/" + theme + "/*",
            "target/wp-content/themes/" + theme + "/**/*",
          ],
        },
        options: {
          watchTask: true,
          proxy: "local." + host + ".com",
        },
      },
    },
    watch: {
      css: {
        files: ["src/css/*.css"],
        tasks: ["concat:css"],
      },
      sass: {
        files: ["src/scss/*.scss", "src/scss/**/*.scss"],
        tasks: ["sass:build"],
      },
      scripts: {
        files: ["src/js/*.js", "src/js/**/*.js"],
        tasks: ["concat:script", "concat:app"],
      },
    },
  });

  grunt.loadNpmTasks("grunt-contrib-concat");
  grunt.loadNpmTasks("grunt-contrib-cssmin");
  grunt.loadNpmTasks("grunt-contrib-watch");
  grunt.loadNpmTasks("grunt-sass");
  grunt.loadNpmTasks("grunt-contrib-uglify-es");
  grunt.loadNpmTasks("grunt-browser-sync");

  grunt.registerTask("dev", [
    "sass:build",
    "concat",
    "cssmin",
    "browserSync",
    "watch",
  ]);

  grunt.registerTask("prod", ["sass:build", "concat", "cssmin", "uglify"]);
  grunt.registerTask("default", ["dev"]);
};
