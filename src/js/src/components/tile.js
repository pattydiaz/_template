var Tile = {
  init: function () {
    Tile.build();
  },
  build: function () {
    if ($(".js-tile").is(":visible")) {
      Tile.animate();
    }
  },
  animate: function () {
    $(".js-tile").each(function () {
      var $this = $(this);
      var $height = "250%";
      var $data = $this.data("position");

      var tileTween = new TimelineMax().add([
        TweenMax.to($this, 0.5, {
          backgroundPosition: $data + "px 0",
          ease: Linear.easeNone,
        }),
      ]);

      var tileScene = new ScrollMagic.Scene({
        triggerElement: $this[0],
        duration: $height,
        offset: -50,
      })
        // .addIndicators()
        .triggerHook(1)
        .setTween(tileTween)
        .addTo(controller);

      w.on("resize", function () {
        $height = "250%";

        tileScene.duration($height);
        tileScene.triggerElement($this[0]);
        tileScene.refresh();
      });
    });
  },
};
