var Parallax = {
  init: function () {
    Parallax.build();
  },
  build: function () {
    if ($(".js-parallax").is(":visible")) {
      Parallax.animate();
    }
  },
  animate: function () {
    $(".js-parallax").each(function () {
      var $this = $(this);
      var $height = "250%";
      var $child = $this.find(".js-parallax-child");
      var $data = $this.data("parallax");

      var parallaxTween = new TimelineMax().add([
        TweenMax.fromTo(
          $child,
          0.5,
          {
            y: -$data + "%",
            ease: Power2.easeInOut,
          },
          {
            y: $data + "%",
            ease: Power1.easeInOut,
          }
        ),
      ]);

      var parallaxScene = new ScrollMagic.Scene({
        triggerElement: $this[0],
        duration: $height,
        offset: -50,
      })
        // .addIndicators()
        .triggerHook(1)
        .setTween(parallaxTween)
        .addTo(controller);

      w.on("resize", function () {
        $height = "250%";

        parallaxScene.duration($height);
        parallaxScene.triggerElement($this[0]);
        parallaxScene.refresh();
      });
    });
  },
};
