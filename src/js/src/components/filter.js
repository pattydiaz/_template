var Filter = {
  init: function () {
    Filter.build();
  },
  build: function () {
    if ($(".filter").is(":visible")) {
      Filter.animate();
    }
  },
  animate: function () {
    var delayInterval = 0.1;
    var filterQuery = location.search.replace("?filter=", "");
    var validQuery = $('.filter-anchor[data-filter="' + filterQuery + '"]');

    function noFilterQuery() {
      $(".filter-results-item").each(function (index) {
        var wineItem = $(this);
        var delay = index * delayInterval;
        wineItem.css({ "transition-delay": "" + delay + "s" });
        wineItem.addClass("active");
      });
    }

    function hasFilterQuery() {
      validQuery.addClass("active");
      // won't trigger click without setTimeout
      setTimeout(function () {
        validQuery.trigger("click");
      }, 0);
    }

    if (validQuery.length > 0 && filterQuery != "") {
      // console.log('valid');
      hasFilterQuery();
    } else {
      // console.log('invalid');
      noFilterQuery();
    }

    $(".filter .filter-anchor").click(function (e) {
      e.preventDefault();

      var thisItem = $(this);
      $(".filter .filter-anchor").removeClass("active");
      thisItem.addClass("active");

      var thisFilter = thisItem.attr("data-filter");
      var matchedFilter = $(
        '.filter-results-item[data-filters*="' + thisFilter + '"]'
      );

      $(".filter-results-item.active").each(function (index) {
        var activeItem = $(this);
        var delay = index * delayInterval;
        activeItem.css({ "transition-delay": "" + delay + "s" });
      });

      $(".filter-results-item").removeClass("active");

      setTimeout(function () {
        $(".filter-results-item").addClass("hide");
      }, 400);

      setTimeout(function () {
        matchedFilter.removeClass("hide");
      }, 450);

      setTimeout(function () {
        matchedFilter.addClass("active");
        matchedFilter.each(function (index) {
          var matchedItem = $(this);
          var delay = index * delayInterval;
          matchedItem.css({ "transition-delay": "" + delay + "s" });
        });
      }, 500);

      // update URL but don't save history states
      history.replaceState(
        "",
        document.title,
        window.location.pathname + "?filter=" + thisFilter
      );
    });
  },
};
