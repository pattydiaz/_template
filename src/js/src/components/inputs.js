var Inputs = {
  init: function () {
      Inputs.build();
  },
  build: function () {
    $(".input-field").each(function () {
      var $this = $(this);
      var $parent = $this.parents('.input');
      $this
        .focusin(function () {
          $parent.addClass("active");
        })
        .focusout(function () {
          if ($this.val() == "") {
            $parent.removeClass("active");
          }
        });
    });
  },
};
