//-----------------
//  Slider
//-----------------

var slideSlider;

var SLIDER = {
  init: function () {
    SLIDER.build();
  },
  build: function () {
    SLIDER.slide();
  },
  slide: function () {
    if ($(".slide-slider").is(":visible")) {
      $(".slide-slider").each(function () {
        var $this = $(this);
        var $swiper = $this.find(".swiper-container");

        var slide = $swiper,
          slideOptions = {
            init: false,
            speed: 600,
            loop: false,
            slidesPerView: "auto",
            watchOverflow: true,
            pagination: {
              el: $this.find(".swiper-pagination"),
              clickable: true,
            },
            navigation: {
              nextEl: $this.find(".swiper-button-next"),
              prevEl: $this.find(".swiper-button-prev"),
            },
            on: {
              init: function() {
                count();
              },
              resize: function() {
                count();
              }
            }
          };

        slideSlider = new Swiper(slide, slideOptions);

        if(!html.hasClass('touch')) {
          slideSlider.params.noSwiping = true;
        } else {
          slideSlider.params.noSwiping = false;
        }

        slideSlider.init();

        function count() {
          var items = $swiper.find('.swiper-slide').length
          
          if(ww >= 768 && ww < 992 && items < 2 || ww >= 992 && items < 3 ) {
            $swiper.addClass('swiper-center')
          } else {
            $swiper.removeClass('swiper-center')
          }
        }
      });
    }
  },
};
