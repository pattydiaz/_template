//-----------------
//  Global
//-----------------

function cl(e) {
  console.log(e);
}

function rtn(e) {
  return e;
}

var w = $(window)
var ww = w.width();
var wh = w.height();
var html = $('html');
var body = $('body');
var page = $('#page');
var main = $('#main');
var content = $('#content');

if(!main.is(':visible')) {
  main = body;
  body.removeClass('css-transitions-off');
  body.addClass('loaded');
}

w.on('resize',function(){
  ww = w.width();
  wh = w.height();

  body.addClass('css-transitions-off');

  setTimeout(function(){
    body.removeClass('css-transitions-off');
  }, 400);
})

var COMP = {
  init: function() {
    COMP.build();
  },
  build: function() {
    COMP.mobile();
    COMP.scroll();
    COMP.page();
    COMP.browser();
    COMP.height();

    w.on("resize", function() {
      COMP.mobile();
      COMP.height();
    });
  },
  mobile: function() {
    $.browser.device = /android|webos|iphone|ipad|ipod|blackberry|iemobile|opera mini/i.test(
      navigator.userAgent.toLowerCase()
    );

    if ($.browser.device == true) {
      body.addClass("mobile");
    } else {
      if (body.hasClass("mobile")) {
        body.removeClass("mobile");
      }
    }
  },
  scroll: function() {
    // default scrolling for apple
    if (!navigator.userAgent.match(/(Android)/)) {
      $("body *").filter(function() {
        if ($(this).css("overflow") == "scroll" || $(this).css("overflow-x") == "scroll" || $(this).css("overflow") == "overlay" || $(this).css("overflow-x") == "overlay") {
          $(this).attr("style", "-webkit-overflow-scrolling: touch !important;");
        }
      });
    }
  },
  page: function() {
    if(content.is(':visible')) {
      var className = content.attr('class');
      var classList = content.attr('class').split(/\s+/);
      
      $.each(classList, function(index, item){
        if(className) {
            item = item.replace('-page','');
            body.addClass(item);
          }
      })
    }
  },
  browser: function() {
    // Opera 8.0+
    var isOpera = (!!window.opr && !!opr.addons) || !!window.opera || navigator.userAgent.indexOf(' OPR/') >= 0;
    // Firefox 1.0+
    var isFirefox = typeof InstallTrigger !== 'undefined';
    // Safari 3.0+ "[object HTMLElementConstructor]" 
    var isSafari = /constructor/i.test(window.HTMLElement) || (function (p) { return p.toString() === "[object SafariRemoteNotification]"; })(!window['safari'] || (typeof safari !== 'undefined' && safari.pushNotification));
    // Internet Explorer 6-11
    var isIE = /*@cc_on!@*/false || !!document.documentMode;
    // Edge 20+
    var isEdge = !isIE && !!window.StyleMedia;
    // Chrome 1 - 79
    var isChrome = !!window.chrome && (!!window.chrome.webstore || !!window.chrome.runtime);
    // Edge (based on chromium) detection
    var isEdgeChromium = isChrome && (navigator.userAgent.indexOf("Edg") != -1);
    // Blink engine detection
    var isBlink = (isChrome || isOpera) && !!window.CSS;

    if(isOpera)
      html.addClass('opera');
      
    if(isFirefox) 
      html.addClass('firefox');

    if(isSafari)
      html.addClass('safari');
      
    if(isIE) 
      html.addClass('ie');

    if(isEdge)
      html.addClass('edge')

    if(isChrome)
      html.addClass('chrome')
    
    if(isEdgeChromium)
      html.addClass('edgeChromium')
    
    if(isBlink)
      html.addClass('blink')
  },
  height: function() {
    var ch = content.height();
    var fh = $('footer').height();
    var gh = $('.grid').height();

    if(gh == undefined) {
      gh = 0
    }

    var dh = ch + fh + gh;

    if(dh < wh) {
      var nh = wh - dh;
      content.css('min-height',ch+nh);
    } else {
      content.attr('style','');
    }
  }
};

//-----------------
//  Keycodes
//-----------------

var KEYCODES = {
  spacebar : 32,
  tab :	9,
  enter :	13,
  esc :	27
}