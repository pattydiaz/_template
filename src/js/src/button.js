//-----------------
//  Buttons
//-----------------

var BUTTON = {
  init: function () {
    BUTTON.build();
  },
  build: function () {
    BUTTON.focus();
  },
  focus: function () {
    $("a, button").on("mouseup", function () {
      $(this).blur();
    });
  },
  doubleclick: function (element) {
    //if already clicked return TRUE to indicate this click is not allowed
    if (element.data("isclicked")) return true;

    //mark as clicked for 1 second
    element.data("isclicked", true);
    setTimeout(function () {
      element.removeData("isclicked");
    }, 1000);

    //return FALSE to indicate this click was allowed
    return false;
  },
};
