//-----------------
//  Init
//-----------------

var CT = {
  init: function () {
    CT.load();
    CT.build();
  },
  load: function () {
    COMP.init(); 
    BUTTON.init();
  },
  build: function () {
    NAV.init();
    COMPONENTS.init();
    MODULES.init();
    SLIDER.init();
    NEWSLETTER.init();

    PAGE.visible(0);
    CT.animate(0);
  },
  animate: function (e) {
    setTimeout(function () {
      body.removeClass("css-transitions-off");
      body.addClass("loaded");
      SCROLLING.init();
    }, e);
  },
};

CT.load();

$(document).ready(function () {
  CT.build();
});