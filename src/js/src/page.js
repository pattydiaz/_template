//-----------------
//  Pages
//-----------------

var PAGE = {
  hidden: function () {
    page.addClass("hidden");
  },
  visible: function (event) {
    setTimeout(function () {
      page.removeClass("hidden");
    }, event);
  },
};
