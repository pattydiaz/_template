var Agegate = {
  init: function () {
      Agegate.build();
  },
  build: function () {
    if ($(".agegate").is(":visible")) {
      Agegate.open();
      Agegate.validate();
      Agegate.overflow();

      w.on("resize", function () {
        Agegate.overflow();
      });

    }
  },
  open: function () {
    if (Cookies.get("agegate") == undefined && !content.hasClass("default-page")) {
      Agegate.show();
    }
  },
  validate: function () {
    var btn_yes = $("#agegate_yes");
    var btn_no = $("#agegate_no");;
    var denied = $(".js-denied");

    btn_no.on("click", function () {
      denied.removeClass("hidden");
      
      setTimeout(function() {
        if(!denied.hasClass('hidden')) {
          denied.addClass('hidden')
        }
      }, 4000);
    });

    btn_yes.on('click',function(){
      if(!denied.hasClass('hidden')) {
        denied.addClass('hidden')
      }
      Agegate.hide();
    });
  },
  show: function() {
    SCROLLING.lock();

    setTimeout(function () {
      body.addClass('agegate-active');
      $(".agegate").removeClass("agegate--hidden");
      $(".agegate").attr("tabindex", "1");
    }, 600);
  },
  hide: function () {
    Cookies.set("agegate", "TRUE", { expires: 7 });
    SCROLLING.unlock();

    body.removeClass('agegate-active');
    $(".agegate").addClass("agegate--hidden");
    $(".agegate").attr("tabindex", "-1");
  },
  overflow: function () {
    var scrollHeight = $(".agegate-container")[0].scrollHeight + 350;

    if (scrollHeight > wh) {
      $(".agegate").addClass("agegate--scroll");
    } else {
      $(".agegate").removeClass("agegate--scroll");
    }
  },
};
