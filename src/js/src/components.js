//-----------------
//  Components
//-----------------

var COMPONENTS = {
  init: function () {
    COMPONENTS.build();
  },
  build: function () {
    Parallax.init();
    Inputs.init();
    Filter.init();
    Tile.init();
  }
};