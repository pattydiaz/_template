//-----------------
//  Nav
//-----------------
var header = $(".header");
var btn = $('.header-btn')
var nav = $('.nav');

var NAV = {
  init: function () {
    NAV.build();
  },
  build: function () {
    NAV.button();
    NAV.overflow();
    NAV.scroll();

    w.on("resize", function () {
      NAV.overflow();
    });
  },
  button: function () {
    $(".header-btn").on("click", function (e) {
      e.preventDefault();
      var button = $(this);

      if (BUTTON.doubleclick(button)) {
        return;
      }

      btn.toggleClass("header-btn--active");

      if (btn.hasClass("header-btn--active")) {
        btn.find('.header-btn-inner').text('Close');
        NAV.animate();

      } else {
        btn.find('.header-btn-inner').text('Menu');
        NAV.close();
      }
    });

    nav.on('click',function() {
      btn.removeClass("header-btn--active");
      btn.find('.header-btn-inner').text('Menu');
      NAV.close();
    });

    nav.find('.nav-wrap').on('click', function(e) {
      e.stopPropagation();
    })
  },
  animate: function () {
    body.addClass('nav-active');
    main.attr('tabindex','-1');
    
    nav.attr('tabindex','1');
    nav.removeClass('nav--hidden');
    nav.addClass('nav--active');
  },
  close: function () {
    body.removeClass('nav-active');
    
    nav.attr('tabindex','-1');
    nav.removeClass('nav--active');
    setTimeout(function(){
      nav.addClass('nav--hidden');
    }, 400);
  },
  overflow: function () {
    if ($("nav").is(":visible")) {
      var sh = $(".nav-container")[0].scrollHeight;
      var nh = sh + $("header").height();

      if (nh > wh) {
        $(".nav").addClass("nav--scroll");
      } else {
        $(".nav").removeClass("nav--scroll");
      }
    }
  },
  scroll: function () {
    var $height = main[0].scrollHeight;

    navScrollTween = TweenMax.to(header, 0.4, {
      y: "-100%",
      ease: Power1.easeInOut,
    }).reverse();

    var navScrollScene = new ScrollMagic.Scene({
      triggerElement: $("section").first(),
      duration: $height,
    })
      // .addIndicators()
      .triggerHook(0)
      .addTo(controller);

    navScrollScene.on("progress", function (e) {
      if(e.progress > 0.001) {
        header.addClass('header--scroll');
      } else {
        header.removeClass('header--scroll');
      }

      if (e.progress > 0.001 && e.scrollDirection == "FORWARD") {
        if(!nav.hasClass('nav--active')) {
          navScrollTween.play();
        }
      }

      if (e.scrollDirection == "REVERSE") {
        navScrollTween.reverse();
      }
    });

    header.find("button").focusin(function () {
      navScrollTween.reverse();
    });
    

    body.attr("data-height", $height);

    main.on("scroll", function () {
      updateHeight();
    });

    function updateHeight() {
      var $newheight = main[0].scrollHeight;

      if ($newheight != $height) {
        body.attr("data-height", $newheight);
        navScrollScene.duration($newheight);
        navScrollScene.refresh();
      }
    }
  },
};