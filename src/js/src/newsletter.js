var NEWSLETTER = {
  init: function () {
    NEWSLETTER.build();
  },
  build: function () {
    if ($("#mc-form").is(":visible")) {
      $("#mc-form").ajaxChimp({
        url: "https://infiniumspirits.us7.list-manage.com/subscribe/post?u=702cf59c0b811d89e92785426&amp;id=26751788f5",
        callback: callbackFunction,
      });

      function callbackFunction(resp) {
        if (resp.result === "success") {
          $("#mc-form").remove();
          $(".footer-newsletter").append(
            "<div class='input-alert valid py-2'>Thanks for joining!</div>"
          );
        }
      }
    }
  },
};
