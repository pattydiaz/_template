//-----------------
//  Scrolling
//-----------------

var controller = new ScrollMagic.Controller({
  container: main,
});

var SCROLLING = {
  init: function () {
    SCROLLING.build();
  },
  build: function () {
    SCROLLING.animate();
  },
  animate: function () {
    // general scrolling animation
    $(".animated").each(function () {
      var $this = $(this);
      var $height = $this.height();

      var scene = new ScrollMagic.Scene({
        triggerElement: $this[0],
        duration: $height,
      })
        // .addIndicators()
        .triggerHook(0.5)
        .addTo(controller);

      scene.on("enter", function () {
        $this.addClass("active");
      });

      scene.on("leave", function (e) {
        if (
          e.scrollDirection == "REVERSE" &&
          $this.hasClass("animated--reverse")
        ) {
          $this.removeClass("active");
        }
      });

      w.on("resize", function () {
        $height = $this.height();

        scene.duration($height);
        scene.triggerElement($this[0]);
        scene.refresh();
      });
    });
  },
  lock: function () {
    main.css("overflow", "hidden");
  },
  unlock: function () {
    main.attr("style", "");
    COMP.scroll();
  },
};
