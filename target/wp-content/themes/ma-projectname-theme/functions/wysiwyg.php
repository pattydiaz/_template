<?php

// color options
function color_mce4_options( $init ) {
    $default_colours = '
        "000000", "Black",
        "FFFFFF", "White",
    ';
    $custom_colours = '';

    $init['textcolor_map'] = '['.$default_colours.','.$custom_colours.']';
    $init['textcolor_rows'] = 2;
    $init['textcolor_cols'] = 6;
    return $init;
}
add_filter('tiny_mce_before_init', 'color_mce4_options');


/**
 * Remove the Color Picker plugin from tinyMCE. This will
 * prevent users from adding custom colors. Note, the default color
 * palette is still available (and customizable by developers) via
 * textcolor_map using the tiny_mce_before_init hook.
 * 
 * @param array $plugins An array of default TinyMCE plugins.
 */
add_filter( 'tiny_mce_plugins', 'wpse_tiny_mce_remove_custom_colors' );
function wpse_tiny_mce_remove_custom_colors( $plugins ) {       

    foreach ( $plugins as $key => $plugin_name ) {
        if ( 'colorpicker' === $plugin_name ) {
            unset( $plugins[ $key ] );
            return $plugins;            
        }
    }

    return $plugins;            
}

?>