<?php 

// custom gallery for posts

add_shortcode('gallery', 'my_gallery_shortcode');    
function my_gallery_shortcode($attr) {
    $post = get_post();

static $instance = 0;
$instance++;

if ( ! empty( $attr['ids'] ) ) {
    // 'ids' is explicitly ordered, unless you specify otherwise.
    if ( empty( $attr['orderby'] ) )
        $attr['orderby'] = 'post__in';
    $attr['include'] = $attr['ids'];
}

// Allow plugins/themes to override the default gallery template.
$output = apply_filters('post_gallery', '', $attr);
if ( $output != '' )
    return $output;

// We're trusting author input, so let's at least make sure it looks like a valid orderby statement
if ( isset( $attr['orderby'] ) ) {
    $attr['orderby'] = sanitize_sql_orderby( $attr['orderby'] );
    if ( !$attr['orderby'] )
        unset( $attr['orderby'] );
}

extract(shortcode_atts(array(
    'order'      => 'ASC',
    'orderby'    => 'menu_order ID',
    'id'         => $post->ID,
    'itemtag'    => 'dl',
    'icontag'    => 'dt',
    'captiontag' => 'dd',
    'columns'    => 3,
    'size'       => 'full',
    'include'    => '',
    'exclude'    => ''
), $attr));

$id = intval($id);
if ( 'RAND' == $order )
    $orderby = 'none';

if ( !empty($include) ) {
    $_attachments = get_posts( array('include' => $include, 'post_status' => 'inherit', 'post_type' => 'attachment', 'post_mime_type' => 'image', 'order' => $order, 'orderby' => $orderby) );

    $attachments = array();
    foreach ( $_attachments as $key => $val ) {
        $attachments[$val->ID] = $_attachments[$key];
    }
} elseif ( !empty($exclude) ) {
    $attachments = get_children( array('post_parent' => $id, 'exclude' => $exclude, 'post_status' => 'inherit', 'post_type' => 'attachment', 'post_mime_type' => 'image', 'order' => $order, 'orderby' => $orderby) );
} else {
    $attachments = get_children( array('post_parent' => $id, 'post_status' => 'inherit', 'post_type' => 'attachment', 'post_mime_type' => 'image', 'order' => $order, 'orderby' => $orderby) );
}

    if ( empty($attachments) )
        return '';

    if ( is_feed() ) {
        $output = "\n";
        foreach ( $attachments as $att_id => $attachment )
            $output .= wp_get_attachment_link($att_id, $size, true) . "\n";
        return $output;
    }

    $captiontag = tag_escape($captiontag);
    $valid_tags = wp_kses_allowed_html( 'post' );

    if ( ! isset( $valid_tags[ $captiontag ] ) )
        $captiontag = 'dd';

    $selector = "gallery-{$instance}";

    $gallery_style = $gallery_div = '';

    $gallery_div =
        "<div class='gallery slide-slider'>
            <div id='$selector' class='gallery-container slide-slider-items galleryid-{$id} swiper-container swiper-no-swiping'>
                <div class='swiper-wrapper'>";

    $output = 
        apply_filters( 'gallery_style', $gallery_style . "\n\t\t" . $gallery_div );

    // $i = 0;
    
    foreach ( $attachments as $id => $attachment ) {
        $image = 
            wp_get_attachment_image($id, 'full', false, false);

        $output .=  
            "<div class='gallery-item slide-slider-item swiper-slide'>";

        $output .=  
            "<div class='gallery-image bg-tan-light'>
                $image
            </div>";

        if ( $captiontag && trim($attachment->post_excerpt) ) {

            $output .= "
            <div class='gallery-caption caption mt-h'>
                " . wptexturize($attachment->post_excerpt) . "
            </div>";
            
        }

        $output .= "</div>";

    }

    $output .= "
                </div>
            </div>
            <div class='gallery-nav slide-slider-nav'>
                <div class='gallery-pagination slide-slider-pagination swiper-pagination swiper-pagination--red'></div>
                <button class='gallery-next slide-slider-next swiper-next swiper-arrow swiper-arrow--red'>
                    " . file_get_contents( TD . '/img/global/icons/arrow-lg.svg') . "
                </button>
                <button class='gallery-prev slide-slider-prev swiper-prev swiper-arrow swiper-arrow--red'>
                    " . file_get_contents( TD . '/img/global/icons/arrow-lg.svg') . "
                </button>
            </div>
        </div>\n";

return $output;

}