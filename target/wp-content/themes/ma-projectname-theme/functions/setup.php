<?php

// remove_action('wp_head', 'wp_generator');

// theme support
add_theme_support( 'title-tag' );
add_theme_support('post-thumbnails');
// add_post_type_support( 'page', 'excerpt' );

// disable image compression
add_filter('jpeg_quality', function($arg){return 100;});

// add thumbnail size
add_image_size( 'XS Thumbnail', 50, 50, true );

// add "page-slug" class to body
function add_slug_body_class( $classes ) {
    global $post;
    
    if ( isset( $post ) ) {
        $classes[] = $post->post_type . '-' . $post->post_name;
    } 
    
    return $classes;
}
add_filter( 'body_class', 'add_slug_body_class' );

// check for empty content 
function empty_content($str) {
    return trim(str_replace('&nbsp;','',strip_tags($str,'<img>'))) == '';
}

// add company name to site identity
function register_additional_customizer_settings( $wp_customize ) {
    $wp_customize->add_setting(
        'company_name',
        array(
            'default' => '',
            'type' => 'option', // you can also use 'theme_mod'
            'capability' => 'edit_theme_options',
            'transport' => 'postMessage',
        )
    );

    $wp_customize->add_control( new WP_Customize_Control(
        $wp_customize,
        'company_name',
        array(
            'label'      => __( 'Company Name' ),
            // 'description' => __( 'Description for your field', 'textdomain' ),
            'settings'   => 'company_name',
            'priority'   => 10,
            'section'    => 'title_tagline',
            'type'       => 'text',
        )
    ) );
}
add_action( 'customize_register', 'register_additional_customizer_settings' );

// change excerpt text
// add_filter('gettext', 'wpse22764_gettext', 10, 2);

// function wpse22764_gettext($translation, $original)
// {
//     if ('Excerpt' == $original) {
//         return 'Page Description';
//     } else {
//         $pos = strpos($original, 'Excerpts are optional hand-crafted summaries of your');

//         if ($pos !== false) {
//             return  '<em>This will act as a fallback if the "Yoast SEO" plugin is not active.</em>';
//         }
//     }
//     return $translation;
// }

?>