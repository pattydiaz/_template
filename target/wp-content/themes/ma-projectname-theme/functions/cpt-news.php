<?php

add_action( 'init', 'register_cpt_news' );

function register_cpt_news() {

    $labels = array(
        'name'               => _x( 'Articles', 'news' ),
        'singular_name'      => _x( 'Article', 'news' ),
        'add_new'            => _x( 'Add New Article', 'news' ),
        'add_new_item'       => _x( 'Create New Article', 'news' ),
        'edit_item'          => _x( 'Edit Article', 'news' ),
        'new_item'           => _x( 'New Article', 'news' ),
        'view_item'          => _x( 'View Articles', 'news' ),
        'search_items'       => _x( 'Search Articles', 'news' ),
        'not_found'          => _x( 'No Articles found', 'news' ),
        'not_found_in_trash' => _x( 'No Articles in the trash', 'news' ),
        'parent_item_colon'  => _x( 'Articles Superior:', 'news' ),
        'menu_name'          => _x( 'Articles', 'news' ),
    );

    $args = array(
        'labels'              => $labels,
        'hierarchical'        => true,
        'description'         => "Articles",
        'supports'            => array( 'title','editor','thumbnail','tags','author'),
        // 'taxonomies'          => array('category'),
        'public'              => true,
        'show_ui'             => true,
        'show_in_menu'        => true,
        'menu_position'       => 9,
        'show_in_nav_menus'   => true,
        'publicly_queryable'  => true,
        'exclude_from_search' => false,
        'has_archive'         => false,
        'query_var'           => true,
        'can_export'          => true,
        'rewrite'             => true,
        'menu_icon'           => 'dashicons-id-alt',
        'capability_type'     => 'post'
    );

    register_post_type( 'news', $args );
}

// custom categories 
function register_news_taxonomy() {

    register_taxonomy(
        'article-type',
        'news',
        array(
            'label' => __( 'Type of Article' ),
            'show_admin_column' => true,
            'rewrite' => array( 'slug' => 'article-type' ),
            'hierarchical' => true,
        )
    );
}

add_action( 'init', 'register_news_taxonomy' );


// allow filtering for custom categories
function add_news_taxonomy_filters() {
	global $typenow;
	
	// an array of all the taxonomyies you want to display. Use the taxonomy name or slug
	$taxonomies = array('article-type');
	
	// must set this to the post type you want the filter(s) displayed on
	if( $typenow == 'news' ){
		
		foreach ($taxonomies as $tax_slug) {
			$tax_obj = get_taxonomy($tax_slug);
			$tax_name = $tax_obj->labels->name;
			$terms = get_terms($tax_slug);
			if(count($terms) > 0) {
				echo "<select name='$tax_slug' id='$tax_slug' class='postform'>";
				echo "<option value=''>Show All $tax_name</option>";
				foreach ($terms as $term) { 
					echo '<option value='. $term->slug, $_GET[$tax_slug] == $term->slug ? ' selected="selected"' : '','>' . $term->name .' (' . $term->count .')</option>'; 
				}
				echo "</select>";
			}
		}
	}
}

add_action( 'restrict_manage_posts', 'add_news_taxonomy_filters' );

?>