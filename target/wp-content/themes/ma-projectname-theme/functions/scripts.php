<?php
function template_scripts() {
    wp_deregister_script('jquery');
    
    wp_enqueue_script( 'jQuery', 'https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js', null, null, true );
    wp_enqueue_script( 'scrollmagic', 'https://cdnjs.cloudflare.com/ajax/libs/ScrollMagic/2.0.7/ScrollMagic.min.js', null, null, true );
    wp_enqueue_script( 'scrollmagic-gsap', 'https://cdnjs.cloudflare.com/ajax/libs/ScrollMagic/2.0.7/plugins/animation.gsap.min.js', null, null, true );
    wp_enqueue_script( 'scrollmagic-jquery', 'https://cdnjs.cloudflare.com/ajax/libs/ScrollMagic/2.0.7/plugins/jquery.ScrollMagic.min.js', null, null, true );
    wp_enqueue_script( 'gsap', 'https://cdnjs.cloudflare.com/ajax/libs/gsap/3.5.1/gsap.min.js', null, null, true );
    wp_enqueue_script( 'swiper', 'https://cdnjs.cloudflare.com/ajax/libs/Swiper/5.4.5/js/swiper.min.js', null, null, true );
    wp_enqueue_script( 'jquery-ajaxchimp', 'https://cdnjs.cloudflare.com/ajax/libs/jquery-ajaxchimp/1.3.0/jquery.ajaxchimp.min.js', null, null, true );
    wp_enqueue_script( 'jquery-ajaxchimp', 'https://cdnjs.cloudflare.com/ajax/libs/jquery-ajaxchimp/1.3.0/jquery.ajaxchimp.langs.min.js', null, null, true );
    
    wp_enqueue_script( 'scrollmagic-debug', 'https://cdnjs.cloudflare.com/ajax/libs/ScrollMagic/2.0.7/plugins/debug.addIndicators.min.js', null, null, true );
    
	wp_enqueue_script( 'apps', TD . '/app.js', array('jQuery'), VER, true);
    wp_enqueue_script( 'main', TD . '/main.js', array('jQuery'), VER, true);
    
}
add_action( 'wp_enqueue_scripts', 'template_scripts' );

?>