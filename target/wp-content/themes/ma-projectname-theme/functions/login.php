<?php 

add_action('login_head', function() {
  remove_action('login_head', 'wp_shake_js', 12);
  echo '<link rel="stylesheet" href="'.TD.'/admin.css?v='.VER.'">';
});

function change_wp_login_url() {
    return get_bloginfo('url');
}
add_filter('login_headerurl', 'change_wp_login_url');

function my_login_head() {
    remove_action('login_head', 'wp_shake_js', 12);
}
add_action('login_head', 'my_login_head');


// change lost password text
function hook_lost_your_password ( $text ) {
    if ($text == 'Lost your password?'){
        $text .= '<br /><a href="https://makersandallies.com" target="_blank">Made by Makers & Allies</a>';
    }
return $text;
}
add_filter( 'gettext', 'hook_lost_your_password' );
