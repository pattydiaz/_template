<?php
    function gb_comment_form_tweaks ($fields) {
        //add placeholders and remove labels
        $fields['author'] = '<div class="input input-half"><input id="author" class="input-field" name="author" value="" placeholder="Name*" equired="required" type="text"></div>';

        $fields['email'] = '<div class="input input-half"><input id="email" class="input-field" name="email" type="email" value="" placeholder="Email*" aria-describedby="email-notes" required="required"></div>';
    
        //unset comment so we can recreate it at the bottom
        unset($fields['comment']);
        $fields['comment'] = '<div class="input input-full"><textarea id="comment" class="input-textarea" name="comment" rows="6" maxlength="5000" placeholder="Comments" required="required"></textarea></div>';
    
        //remove website
        unset($fields['url']);
    
        return $fields;
    }
    
    add_filter('comment_form_fields', 'gb_comment_form_tweaks');
    
?>