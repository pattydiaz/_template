<?php

// remove Ninja Form & Share Icons from Page, Post, etc.
add_action('add_meta_boxes', function() {
    //ninjaform
    remove_meta_box('nf_admin_metaboxes_appendaform', ['page', 'post'], 'side');
    //shareicons
    remove_meta_box('A2A_SHARE_SAVE_meta', ['page', 'post'], 'side');
}, 99);


?>