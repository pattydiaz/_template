<?php 
// custom menu and footer
if( function_exists('acf_add_options_page') ) {
		
	acf_add_options_sub_page(array(
		'page_title' 	=> 'Menu',
		'menu_title'	=> 'Menu',
		'menu_slug'	    => 'menu',
        'capability'	=> 'edit_posts',
        'parent_slug'   => 'themes.php',
        'position'      => false,
        'icon_url'      => false
    ));
    
	acf_add_options_sub_page(array(
		'page_title' 	=> 'Footer',
		'menu_title'	=> 'Footer',
		'menu_slug'	    => 'footer',
        'capability'	=> 'edit_posts',
        'parent_slug'   => 'themes.php',
        'position'      => false,
        'icon_url'      => false
	));
    
	acf_add_options_sub_page(array(
		'page_title' 	=> 'Content Blocks',
		'menu_title'	=> 'Content Blocks',
		'menu_slug'	    => 'content-blocks',
        'capability'	=> 'edit_posts',
        'parent_slug'   => 'themes.php',
        'position'      => false,
        'icon_url'      => false
	));
	
}
?>