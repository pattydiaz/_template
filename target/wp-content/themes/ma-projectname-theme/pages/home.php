<?php /* Template Name: Home Page Template */ ?>
<?php get_header(); ?>

<div id="content" class="home-page">

    <!-- Hero Section -->
    <section class="hero section section--padding">
        <div class="hero-container container container-md">
            <div class="hero-text section-text text-center">
                <h1 class="hero-text-title section-text-title title-xl"><?php the_title();?></h1>
            </div>
        </div>
    </section>
    

    <!-- Content Section -->
    <section class="content section">
        <div class="content-container container container-md">
            <div class="content-text section-content text">
                <?php the_content(); ?>
            </div>
        </div>
    </section>
    
</div>

<?php get_footer(); ?>