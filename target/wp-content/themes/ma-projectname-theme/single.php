<?php get_header(); ?>

<div id="content" class="default-page">

    <!-- Hero Section -->
    <section class="hero section section--padding bg-paper">
        <div class="hero-container container container-md">
            <div class="hero-text section-text-half">
                <h1 class="hero-title section-title title-xl"><?php the_title();?></h1>
            </div>
        </div>
    </section>
    

    <!-- Content Section -->
    <section class="content section bg-paper">
        <div class="content-container container container-lg">
            <div class="content-text section-content text">
                <?php if ( have_posts() ) : while ( have_posts() ) : the_post();?>
                    <?php if (empty_content($post->post_content)): ?>
                        <div class="text-center">
                            <h4 class="pb-2">There is currently no content.</h4>
                            <a href="<?php echo URL;?>" class="btn btn-underline">Back to Home Page</a>
                        </div>
                    <?php else : the_content(); endif; ?>
                <?php endwhile; endif; ?>
            </div>
        </div>
    </section>
    
</div>

<?php get_footer(); ?>