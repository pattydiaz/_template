<?php get_header(); ?>

<div id="content" class="default-page">

    <!-- Hero Section -->
    <section class="hero section section--padding bg-paper">
        <div class="hero-container container container-lg">
            <div class="hero-text section-text-half color-gold">
                <h1 class="hero-title section-title title-xl"><?php the_title();?></h1>
            </div>
        </div>
    </section>
    

    <!-- Content Section -->
    <section class="content section bg-paper">
        <div class="content-container container container-lg">
            <div class="content-text section-content text">
            <?php if(is_page( 'Sitemap' )):?>
                <ul class="page_items">
                    <?php wp_list_pages( array( 'title_li' => '', ) ); ?>
                </ul>
            <?php else:?>

                <?php if ( have_posts() ) : while ( have_posts() ) : the_post();?>
                    <?php if (empty_content($post->post_content)): ?>
                        <div class="text-center">
                            <h4 class="pb-2">There is currently no content.</h4>
                            <a href="<?php echo URL;?>" class="btn btn-underline color-dust" style="--bg: var(--dust);">Back to Home Page</a>
                        </div>
                    <?php else : the_content(); endif; ?>
                <?php endwhile; endif; ?>

            <?php endif;?>
            </div>
        </div>
    </section>
    
</div>

<?php get_footer(); ?>