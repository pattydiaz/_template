<!DOCTYPE html>
<html <?php language_attributes(); ?>>

<head>

	<meta charset="<?php bloginfo('charset'); ?>">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">

	<?php define('VER', rand(0, 88888888)); ?>
	<link rel="stylesheet" type="text/css" href="<?php echo TD; ?>/app.css?v=<?php echo VER; ?>">
	<link rel="stylesheet" type="text/css" href="<?php echo TD; ?>/style.css?v=<?php echo VER; ?>">

	<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
	<meta name="apple-mobile-web-app-capable" content="yes" />
	<meta name="application-name" content="Projectname" />
	<meta name="apple-mobile-web-app-title" content="Projectname">
	<meta name="apple-mobile-web-app-status-bar-style" content="black">

	<?php wp_head(); ?>

</head>

<body <?php body_class('css-transitions-off'); ?>>

	<div id="page" class="hidden">

		<?php get_template_part('modules/header'); ?>
		<?php get_template_part('modules/nav'); ?>

		<main id="main" tabindex="0">