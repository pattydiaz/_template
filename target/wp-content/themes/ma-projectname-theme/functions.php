<?php

define('TD', ''.get_template_directory_uri().'');
define('URL', get_bloginfo( 'url' ));
define('VER', rand(0,88888888));

// wordpress functions
require 'functions/login.php';
require 'functions/scripts.php';
require 'functions/setup.php';
require 'functions/acf.php';
require 'functions/options.php';
require 'functions/wysiwyg.php';

?>