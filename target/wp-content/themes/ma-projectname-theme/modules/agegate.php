<div class="agegate hidden" tabindex="-1">

    <div class="agegate-container container container-md text-center">

        <div class="agegate-logo"></div>
        
        <h1 class="agegate-title title-md mb-2">Are you of legal drinking age?</h1>

        <form>
            <fieldset id="agegate" class="agegate-inputs input-group color-tan">
                <div class="input">
                    <input id="agegate_yes" type="radio" value="yes" name="agegate" class="input-radio">
                    <label for="agegate_yes" class="input-label ml-1 mr-4">Yes</label>
                </div>
                <div class="input">
                    <input id="agegate_no" type="radio" value="no" name="agegate" class="input-radio">
                    <label for="agegate_no" class="input-label ml-1">No</label>
                </div>
            </fieldset>

            <div class="agegate-info caption">By clicking Enter, you accept our site's use of cookies. More information can be found in our Privacy Policy.</div>

            <div class="agegate-access">
                <a href="#enter" class="btn btn-rectangle btn-rectangle--bg bg-gold agegate-access-item js-enter hidden" title="Enter"><span>Enter</span></a>

                <div class="agegate-access-item js-denied hidden">
                    <div class="mb-2 text-uppercase">You are not old enough to visit this site.</div>
                    <a href="" class="btn btn-rectangle btn-rectangle--bg bg-gold" title="" target="">Find Out More</a>
                </div>
            </div>
        </form>    
    </div>

    <!-- Legal Navigation -->
    <ul class="agegate-legal caption text-uppercase text-center">
        <li class="agegate-legal-item">&copy; <?php echo date('Y');?> <?php echo get_option( 'company_name' ); ?> All Rights Reserved</li>
        <li class="agegate-legal-item"><a href="#" title="">Privacy Policy</a></li>
        <li class="agegate-legal-item"><a href="#" title="">Terms &amp; Conditions</a></li>
    </ul>
</div>