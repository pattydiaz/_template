<div class="promo hidden" tabindex="-1">
    <div class="promo-container">
        <div class="promo-content">
            <a href="#" class="promo-close btn-close"></a>
            <div class="promo-content-image">
                <img src="" alt="">
            </div>
            <div class="promo-content-text">
                <div class="text">
                    <div class="title"></div>
                    <div class="info"></div>
                    <a href="#" class="btn" target="_blank"></a>
                </div>
            </div>
        </div>
    </div>
</div>