<div class="toggle mt-10">
    <div class="toggle-group bg-black my-2">
        <div class="toggle-group-items">
            <button data-toggle="three" class="toggle-group-item subtitle-lg active">3 Bottle</button>
            <button data-toggle="six" class="toggle-group-item subtitle-lg">6 Bottle</button>
            <button data-toggle="twelve" class="toggle-group-item subtitle-lg">12 Bottle</button>
        </div>
    </div>
    <div class="toggle-group bg-green-cave bg-grit my-2">
        <div class="toggle-group-items">
            <button data-toggle="reds" class="toggle-group-item subtitle-lg active">Reds Only</button>
            <button data-toggle="mixed" class="toggle-group-item subtitle-lg">Reds &amp; Whites</button>
        </div>
    </div>

    <div class="toggle-links mt-8">
        <div data-link="three-reds" class="toggle-links-item">
            <a href="#three-reds" class="btn btn-rectangle btn-rectangle--bg bg-gold"><span>Join the Club</span></a>
        </div>
        <div data-link="three-mixed" class="toggle-links-item">
            <a href="#three-mixed" class="btn btn-rectangle btn-rectangle--bg bg-gold"><span>Join the Club</span></a>
        </div>
        <div data-link="six-reds" class="toggle-links-item">
            <a href="#six-reds" class="btn btn-rectangle btn-rectangle--bg bg-gold"><span>Join the Club</span></a>
        </div>
        <div data-link="six-mixed" class="toggle-links-item">
            <a href="#six-mixed" class="btn btn-rectangle btn-rectangle--bg bg-gold"><span>Join the Club</span></a>
        </div>
        <div data-link="twelve-reds" class="toggle-links-item">
            <a href="#twelve-reds" class="btn btn-rectangle btn-rectangle--bg bg-gold"><span>Join the Club</span></a>
        </div>
        <div data-link="twelve-mixed" class="toggle-links-item">
            <a href="#twelve-mixed" class="btn btn-rectangle btn-rectangle--bg bg-gold"><span>Join the Club</span></a>
        </div>
    </div>
</div>